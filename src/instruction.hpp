/* This file is part of MAPS.
 *
 * MAPS is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * MAPS is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * MAPS.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once
#include <iosfwd>

namespace maps {

enum Instruction : unsigned {
    CLEAR = 0x0,
    ADD_L = 0x1, // Add literal.
    ADD_R = 0x2, // Add registers.
    SUB_L = 0x3, // Subtract literal.
    SUB_R = 0x4, // Subtract registers.
    JMP = 0x5,
    JMP_R = 0x6,
    BOZ = 0x7, // Branch when greater than 0.
    BNE = 0x8, // Branch on zero.
    LOAD = 0x9, // Load from main memory THEN increment pointer register.
    STORE = 0xA, // Decrement pointer register THEN store in main memory.
    MOV = 0xB, // Move between registers.
    MOV_L = 0xC,
    AND = 0xD,
    OR = 0xE,
    NOT = 0xF
    
};

std::ostream& operator<< (std::ostream& os, Instruction in);
std::string to_string(Instruction in);

} // namespace maps
