#pragma once
#include <QSyntaxHighlighter>

namespace maps {
namespace gui {

class SyntaxHighlighter : public QSyntaxHighlighter
{
    Q_OBJECT

public:
    SyntaxHighlighter(QTextDocument* parent = 0);

protected:
    void highlightBlock(const QString& text) override;

private:
    struct HighlightingRule
    {
        QRegExp pattern;
        QTextCharFormat format;
    };

    QVector<HighlightingRule> rules;

    QRegExp commentStartExpression;
    QRegExp commentEndExpression;

    QTextCharFormat instructionFormat;
    QTextCharFormat singleLineCommentFormat;
    QTextCharFormat multiLineCommentFormat;
    QTextCharFormat registerFormat;
    QTextCharFormat literalFormat;
};

} // namespace gui
} // namespace maps
