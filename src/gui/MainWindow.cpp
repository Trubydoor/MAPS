/* This file is part of MAPS.
 *
 * MAPS is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * MAPS is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * MAPS.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "MainWindow.hpp"
#include <QtWidgets>
#include <QtQuick>
#include "../emulator/cpu.hpp"
#include "../emulator/alu.hpp"
#include "../emulator/controlunit.hpp"
#include "../emulator/mmu.hpp"
#include "../assembler/assembler.hpp"
#include "../language/compiler.hpp"
#include "RegisterList.hpp"
#include "MemoryList.hpp"
#include "InstructionList.hpp"
#include "Editor.hpp"

namespace maps {
namespace gui {

MainWindow::MainWindow() : cpu(new maps::emulator::CPU),
    registerList{new RegisterList},
    memoryList{new MemoryList{&cpu->getMMU().getMemory()}},
    instructionList{new InstructionList},
    cpuDisplay{new QQuickView},
    text{new Editor{this}},
    timer{new QTimer{this}},
    widget{new QWidget{this}},
    timeBox{new QDoubleSpinBox{this}}
{
    QWidget* cpuDisplayWidget = QWidget::createWindowContainer(cpuDisplay);
    initDisplay();
    cpuDisplayWidget->setFixedSize(200,400);
    //text->hide();

    // Widget simply holds a layout with other widgets in it. We need this since
    // a MainWindow needs a central widget to display, and we use this as ours.
    QHBoxLayout* layout = new QHBoxLayout{widget};
    layout->addWidget(text);
    layout->addWidget(instructionList);
    layout->addWidget(cpuDisplayWidget);
    layout->addWidget(registerList);
    layout->addWidget(memoryList);

    setCentralWidget(widget);

    memoryList->hide();
    createMenus();
    createToolbars();

    // Start with an initial value of 0.5s
    timeBox->setValue(0.5);

    QObject::connect(timer, &QTimer::timeout, this, &MainWindow::refresh);

    // Unfortunately we need to use old connect syntax here since we're using
    // an overloaded signal.
    QObject::connect(timeBox, SIGNAL(valueChanged(double)),
                     this, SLOT(setTimerInterval(double)));

    setMinimumSize(sizeHint());
}

MainWindow::~MainWindow() = default;

void MainWindow::initDisplay()
{
    cpuDisplay->rootContext()->setContextProperty("window",this);
    cpuDisplay->rootContext()->setContextProperty("alu",&cpu->getALU());
    cpuDisplay->rootContext()->setContextProperty("cu",&cpu->getCU());
    cpuDisplay->rootContext()->setContextProperty("mmu", &cpu->getMMU());
    cpuDisplay->setSource(QUrl("qrc:/qml/cpu.qml"));
}

void MainWindow::formatText()
{
    QString str = text->toPlainText();
    // Get all the text into a standard format so we can highlight each line
    // as we're processing it.
    str.replace(QRegularExpression(R"(\n\s)"),"\n");
    str.replace(QRegularExpression(R"(:\s)"), ": ");
    text->setPlainText(str);
}

void MainWindow::openFileDialog()
{
    // Pop open a dialog showing only files ending in .s (assembly files).
    QString fileName = QFileDialog::getOpenFileName(this, tr("Open File"), QString(), 
                            tr("Assembly Files (*.s);;Language Files (*.y)"));

    if (!fileName.isEmpty()) {
        QFile file(fileName);

        if (!file.open(QIODevice::ReadOnly)) {
            // File failed to open, throw up a dialog.
            QMessageBox::critical(this, tr("Error"), tr("Could not open file"));
            return;
        }
        
        QTextStream in(&file);
        if (fileName.endsWith(".y")) {
            QString s = in.readAll();
            std::string compiled;
            try {
                compiled = maps::language::compile(s.toUtf8());
            } catch (std::exception& e) {
                QMessageBox::critical(this, // The parent widget.
                                tr("Parse Error!"), // The title of the dialog.
                                e.what(), // The message in the dialog.
                                QMessageBox::Ok, // Buttons to display.
                                QMessageBox::Ok); // Default selected button.
                return;
            }
            text->setPlainText(QString::fromStdString(compiled));
        } else {
            text->setPlainText(in.readAll());
        }
        file.close();
        
        cpu->instruction_memory = maps::assembler::assemble(
                    text->toPlainText().toStdString());
        instructionList->setInstructions(cpu->instruction_memory);
    }
}

void MainWindow::createMenus()
{
    QMenu* fileMenu = menuBar()->addMenu(tr("&File"));

    createButton(fileMenu, tr("&Open file"), this, &MainWindow::openFileDialog);
    createButton(fileMenu, tr("&Quit"), this, &MainWindow::close);
    
    QMenu* viewMenu = menuBar()->addMenu(tr("&View"));
    auto editor = createButton(viewMenu, tr("&Editor"), 
                               this, &MainWindow::editorVisible);
    editor->setCheckable(true);
    editor->setChecked(true);
    QMenu* repMenu = viewMenu->addMenu(tr("&Representation"));
    
    QActionGroup* actionGroup = new QActionGroup{this};
    createButton(actionGroup, tr("&Binary"), this,
                 &MainWindow::setBin);
    createButton(actionGroup, tr("&Octal"), this,
                 &MainWindow::setOct);
    createButton(actionGroup, tr("&Decimal"), this,
                 &MainWindow::setDec);
    QAction* hex = createButton(actionGroup, tr("&Hexidecimal"), this,
                 &MainWindow::setHex);
    
    for (QAction* act : actionGroup->actions()) {
        act->setCheckable(true);
    }
    hex->setChecked(true);
    
    repMenu->addActions(actionGroup->actions());
    
    QAction* act = createButton(viewMenu, tr("&Main Memory"),
                                this, &MainWindow::memoryListVisible);
    act->setCheckable(true);
}

void MainWindow::createToolbars()
{
    QToolBar* toolBar = new QToolBar;
    toolBar->setMovable(false);
    addToolBar(Qt::TopToolBarArea, toolBar);

    createButton(toolBar, QStyle::SP_MediaPlay, tr("Start"), 
                 this, &MainWindow::start);
    createButton(toolBar, QStyle::SP_MediaPause, tr("Pause"), 
                 this, &MainWindow::pause);
    createButton(toolBar, QStyle::SP_MediaStop, tr("Stop"), 
                 this, &MainWindow::stop);

    timeBox->setParent(toolBar);
    timeBox->setMinimum(0);
    timeBox->setMaximum(2);
    timeBox->setSingleStep(0.1);
    timeBox->setSuffix("s");
    toolBar->addWidget(timeBox);
}

void MainWindow::start()
{
    if (! text->isReadOnly()) { // We are only paused if this is read only.
        text->setReadOnly(true);
        try {
            cpu->instruction_memory = maps::assembler::assemble(
                        text->toPlainText().toStdString());
            instructionList->setInstructions(cpu->instruction_memory);
        } catch (std::exception& e) {
            text->setReadOnly(false);
            QMessageBox::critical(this, // The parent widget.
                                  tr("Parse Error!"), // The title of the dialog.
                                  e.what(), // The message in the dialog.
                                  QMessageBox::Ok, // Buttons to display.
                                  QMessageBox::Ok); // Default selected button.
            return;
        }
    }
    timer->start(timeBox->value() * 1000);
}

void MainWindow::pause()
{
    timer->stop();
}

void MainWindow::stop()
{
    text->setReadOnly(false);
    cpu.reset(new emulator::CPU);
    memoryList->setMemory(&cpu->getMMU().getMemory());
    registerList->updateRegisters(cpu->R, cpu->CCR);
    memoryList->updateMemory();
    initDisplay();
    timer->stop();
}

void MainWindow::refresh()
{
    emit stepped();
    emulator::ControlUnit& cu = cpu->getCU();
    if (cpu->PC < cpu->instruction_memory.size()) {
        instructionList->setPCLocation(cpu->PC);
    }
    if (cu.fetch()) {
        cu.decode();
        cu.execute();
        registerList->updateRegisters(cpu->R, cpu->CCR);
        memoryList->updateMemory();
    } else {
        QMessageBox::information(this,
            tr("Finished"),
            tr("Done"),
            QMessageBox::Ok,
            QMessageBox::Ok);
        stop();
    }
}

template <class Widget, typename Fun, class Object>
QAction* MainWindow::createButton(Widget* parent, QString str, Object* obj, Fun f)
{
    QAction* act = new QAction(str,parent);
    QObject::connect(act, &QAction::triggered, obj, f);
    parent->addAction(act);
    return act;
}

template <class Widget, typename Fun, class Object>
QAction* MainWindow::createButton(Widget* parent, QStyle::StandardPixmap icon, QString str,
                      Object* obj, Fun f)
{
    QStyle* style = QApplication::style();
    QIcon ic = style->standardIcon(icon);

    QAction* act = new QAction(ic,str,parent);
    act->setIcon(ic);
    QObject::connect(act, &QAction::triggered, obj, f);
    parent->addAction(act);
    return act;
}

void MainWindow::setTimerInterval(double interval)
{
    bool running = timer->isActive();
    timer->stop();
    timer->setInterval(interval*1000);
    if (running) {
        timer->start();
    }
}

void MainWindow::memoryListVisible(bool visible)
{
    memoryList->setVisible(visible);
    setMinimumSize(sizeHint());
}

void MainWindow::editorVisible(bool visible)
{
    text->setVisible(visible);
    setMinimumSize(sizeHint());
}


void MainWindow::setBin()
{
    registerList->setBin();
    memoryList->setBase(2);
}

void MainWindow::setOct()
{
    registerList->setOct();
    memoryList->setBase(8);
}

void MainWindow::setDec()
{
    registerList->setDec();
    memoryList->setBase(10);
}

void MainWindow::setHex()
{
    registerList->setHex();
    memoryList->setBase(16);
}

} // namespace gui
} // namespace maps
