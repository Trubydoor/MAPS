/* This file is part of MAPS.
 *
 * MAPS is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * MAPS is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * MAPS.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once
#include <vector>
#include <array>
#include <QWidget>
#include "../emulator/cpu.hpp"
#include "../emulator/mmu.hpp"

class QLCDNumber;
class QVBoxLayout;
class QScrollArea;

class QListWidget;
class QTableWidget;

namespace maps {
namespace gui {

class MemoryList : public QWidget
{
    Q_OBJECT

    const emulator::MMU::Memory* memory;
    QTableWidget* table;
    QFont font{"Monospace"};

    int base {16};

public:
    MemoryList(const emulator::MMU::Memory* memory, QWidget* parent = nullptr);
    ~MemoryList();

    void updateMemory();
    void setBase(int base);
    void setMemory(const emulator::MMU::Memory* memory) {this->memory = memory;}
};

} // namespace gui
} // namespace maps
