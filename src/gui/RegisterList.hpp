/* This file is part of MAPS.
 *
 * MAPS is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * MAPS is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * MAPS.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once
#include <array>
#include <cstdint>
#include <bitset>
#include <QWidget>

class QLCDNumber;
class QVBoxLayout;

namespace maps {
namespace gui {

class RegisterList : public QWidget
{
    Q_OBJECT
    
    std::array<QLCDNumber*,16> registers;
    QLCDNumber* ccr;
    QVBoxLayout* layout;

public:
    RegisterList(QWidget* parent = nullptr);
    ~RegisterList();
    
    void updateRegisters(const std::array<std::uint16_t, 16>& registers, std::bitset<2> cpuccr);
    void setHex();
    void setOct();
    void setBin();
    void setDec();
};

} // namespace gui
} // namespace maps
