/* This file is part of MAPS.
 *
 * MAPS is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * MAPS is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * MAPS.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "Editor.hpp"
#include <algorithm>
#include <QTextBlock>
#include <QPainter>
#include <QPointF>
#include "SyntaxHighlighter.hpp"

namespace maps {
namespace gui {

Editor::Editor(QWidget *parent) : QPlainTextEdit(parent),
    highlighter(new SyntaxHighlighter(this->document()))
{
    connect(this, &Editor::blockCountChanged,
            this, &Editor::updateLineNumberAreaWidth);

    connect(this, &Editor::updateRequest,
            this, &Editor::updateLineNumberArea);
    updateLineNumberAreaWidth(0);
}

int Editor::lineNumberAreaWidth()
{
    int digits = 1;
    int max = std::max(1, blockCount());

    // Number of digits in the last line of the editor.
    while (max >= 10) {
        max /= 10;
        ++digits;
    }

    int space = 3 + fontMetrics().width(QLatin1Char('9')) * digits;
    return space;
}

void Editor::updateLineNumberAreaWidth(int /*newBlockCount*/)
{
    setViewportMargins(lineNumberAreaWidth(), 0, 0, 0);
}

void Editor::updateLineNumberArea(const QRect &rect, int dy)
{
    if (dy) {
        lineNumbers->scroll(0, dy);
    } else {
        lineNumbers->update(0, rect.y(), lineNumbers->width(), rect.height());
    }

    if (rect.contains(viewport()->rect())) {
        updateLineNumberAreaWidth(0);
    }
}

void Editor::resizeEvent(QResizeEvent* event)
{
    QPlainTextEdit::resizeEvent(event);
    QRect cr = contentsRect();
    cr.setWidth(lineNumberAreaWidth());
    lineNumbers->setGeometry(cr);
}

void Editor::lineNumberAreaPaintEvent(QPaintEvent* event)
{
    QPainter painter {lineNumbers};
    painter.fillRect(event->rect(), 0xEEEEEE);

    QTextBlock block = firstVisibleBlock();
    int blockNumber = block.blockNumber();
    int top = static_cast<int>(
                blockBoundingGeometry(block).translated(contentOffset()).top());
    int bottom = top + static_cast<int>(blockBoundingRect(block).height());

    while (block.isValid() && top <= event->rect().bottom()) {
        if (block.isVisible() && bottom >= event->rect().top()) {
            QString number = QString::number(blockNumber + 1);
            painter.setPen(Qt::black);
            painter.drawText(0, top, lineNumbers->width(),
                             fontMetrics().height(), Qt::AlignRight, number);
        }

        block = block.next();
        top = bottom;
        bottom = top + static_cast<int>(blockBoundingRect(block).height());
        ++blockNumber;
    }
}

} // namespace gui
} // namespace maps
