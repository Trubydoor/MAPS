/* This file is part of MAPS.
 *
 * MAPS is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * MAPS is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * MAPS.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once
#include <QPlainTextEdit>

namespace maps {
namespace gui {

class SyntaxHighlighter;

class Editor : public QPlainTextEdit
{
    Q_OBJECT

    class LineNumbers : public QWidget
    {
        Editor* editor;
    public:
        LineNumbers(Editor* editor) : QWidget(editor) {
            this->editor = editor;
        }

        QSize sizeHint() const override {
            return QSize(editor->lineNumberAreaWidth(), 0);
        }
    private:
        void paintEvent(QPaintEvent* event) override {
            editor->lineNumberAreaPaintEvent(event);
        }
    };


    QWidget* lineNumbers = new LineNumbers(this);
    SyntaxHighlighter* highlighter;

public:
    Editor(QWidget* parent = nullptr);
    void lineNumberAreaPaintEvent(QPaintEvent* event);
    int lineNumberAreaWidth();

protected:
    void resizeEvent(QResizeEvent* event) override;

private slots:
    void updateLineNumberAreaWidth(int newBlockCount);
    void updateLineNumberArea(const QRect& rect, int dy);

};

} // namespace gui
} // namespace maps
