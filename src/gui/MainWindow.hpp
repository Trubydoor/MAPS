/* This file is part of MAPS.
 *
 * MAPS is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * MAPS is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * MAPS.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once
#include <memory>
#include <QMainWindow>
#include <QStyle>

class QDoubleSpinBox;
class QLabel;
class QTimer;
class QWidget;
class QQuickView;

namespace maps {
namespace emulator {
class CPU;
}

namespace gui {

class RegisterList;
class MemoryList;
class InstructionList;
class Editor;

class MainWindow : public QMainWindow
{
    Q_OBJECT
    std::unique_ptr<maps::emulator::CPU> cpu;
    RegisterList* registerList;
    MemoryList* memoryList;
    InstructionList* instructionList;
    QQuickView* cpuDisplay;
    Editor* text;
    QTimer* timer;
    QWidget* widget;
    QDoubleSpinBox* timeBox;

    void initDisplay();
    void createMenus();
    void createToolbars();
    template <typename Widget, typename Fun, class Object>
    QAction* createButton(Widget* parent, QString str, Object* obj, Fun f);
    template <typename Widget, typename Fun, class Object>
    QAction* createButton(Widget* parent, QStyle::StandardPixmap icon, QString str,
                          Object* obj, Fun f);
    void openFileDialog();
    void formatText();
public:
    MainWindow();
    ~MainWindow();

    void start();
    void pause();
    void stop();
    void refresh();

signals:
    void stepped();

public slots:
    void setTimerInterval(double interval);
    void memoryListVisible(bool visible);
    void editorVisible(bool visible);

    void setBin();
    void setOct();
    void setDec();
    void setHex();
};

} // namespace gui
} // namespace maps
