#include <SyntaxHighlighter.hpp>

namespace maps {
namespace gui {

SyntaxHighlighter::SyntaxHighlighter(QTextDocument* parent) :
    QSyntaxHighlighter(parent)
{
    HighlightingRule rule;

    instructionFormat.setForeground(Qt::darkBlue);
    instructionFormat.setFontWeight(QFont::Bold);
    QStringList instructionPatterns {
        "add","sub","jmp","boz","bne","mov","load","store","clear"
    };
    for (const QString& pattern : instructionPatterns) {
        rule.pattern = QRegExp(pattern);
        rule.format = instructionFormat;
        rules.append(rule);
    }

    literalFormat.setForeground(Qt::darkGreen);
    rule.pattern = QRegExp("(0x|0o|0b)?\\d+");
    rule.format = literalFormat;
    rules.append(rule);

    registerFormat.setForeground(Qt::red);
    rule.pattern = QRegExp("r[0-15]");
    rule.format = registerFormat;
    rules.append(rule);

    singleLineCommentFormat.setForeground(Qt::lightGray);
    rule.pattern = QRegExp("//[^\n]*");
    rule.format = singleLineCommentFormat;
    rules.append(rule);

    multiLineCommentFormat.setForeground(Qt::lightGray);
    commentStartExpression = QRegExp("/\\*");
    commentEndExpression = QRegExp("\\*/");
}

void SyntaxHighlighter::highlightBlock(const QString &text)
{
    for (const HighlightingRule& rule : rules) {
        QRegExp expression(rule.pattern);
        int index = expression.indexIn(text);
        while (index >= 0) {
            int length = expression.matchedLength();
            setFormat(index, length, rule.format);
            index = expression.indexIn(text, index + length);
        }
    }

    setCurrentBlockState(0);

    int startIndex = 0;
    if (previousBlockState() != 1) {
        startIndex = commentStartExpression.indexIn(text);
    }

    while (startIndex >= 0) {
        int endIndex = commentEndExpression.indexIn(text, startIndex);
        int commentLength;
        if (endIndex == -1) {
            setCurrentBlockState(1);
            commentLength = text.length() - startIndex;
        } else {
            commentLength = endIndex - startIndex +
                    commentEndExpression.matchedLength();
        }
        setFormat(startIndex, commentLength, multiLineCommentFormat);
        startIndex = commentStartExpression.indexIn(text,
                                                    startIndex + commentLength);
    }
}

} // namespace gui
} // namespace maps
