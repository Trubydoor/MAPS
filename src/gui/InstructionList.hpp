/* This file is part of MAPS.
 *
 * MAPS is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * MAPS is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * MAPS.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once
#include <QWidget>

class QTableWidget;

namespace maps {
namespace gui {
    
class InstructionList : public QWidget
{
    Q_OBJECT;
    
    QTableWidget* table;
    std::size_t PC {0};
public:
     explicit InstructionList(QWidget* parent = 0);
     void setInstructions(const std::vector<std::uint16_t>& instructions);
     void setPCLocation(const std::size_t newPC);
};

}
}