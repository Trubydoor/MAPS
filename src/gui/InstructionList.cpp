/* This file is part of MAPS.
 *
 * MAPS is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * MAPS is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * MAPS.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "InstructionList.hpp"
#include "../assembler/disassembler.hpp"
#include <QTableWidget>
#include <QVBoxLayout>
#include <QHeaderView>
#include <boost/lexical_cast.hpp>

namespace maps {
namespace gui {
    
InstructionList::InstructionList (QWidget* parent) : QWidget(parent),
    table{new QTableWidget}
{
    table->setColumnCount(2);
    table->setHorizontalHeaderLabels({"Instruction","Arguments"});
    table->horizontalHeader()->show();
    //table->setItem(0,0,new QTableWidgetItem{""});
    table->setVerticalHeaderItem(0, new QTableWidgetItem("0"));
    table->verticalHeader()->show();
    table->setEditTriggers(QAbstractItemView::NoEditTriggers);
    QVBoxLayout* layout = new QVBoxLayout{this};
    layout->addWidget(table);
}

void InstructionList::setInstructions(const std::vector<uint16_t>& instructions)
{
    table->setRowCount(instructions.size());
    for (std::size_t i = 0; i < instructions.size(); ++i) {
        auto dis = assembler::disassemble(instructions[i]);
        QString instruction = QString::fromStdString(to_string(std::get<0>(dis)));
        auto instr = new QTableWidgetItem{instruction};
        auto args = new QTableWidgetItem{QString::fromStdString(std::get<1>(dis))};
        table->setItem(i,0,instr);
        table->setItem(i,1,args);
        table->setVerticalHeaderItem(i, new QTableWidgetItem(
            QString{"%1"}.arg(i))
        );
    }

    for (int i = 0; i < table->rowCount(); ++i) {
        table->setVerticalHeaderItem(i, new QTableWidgetItem(QString::number(i)));
    }
}

void InstructionList::setPCLocation(const std::size_t newPC)
{
    QFont font;
    font.setBold(false);
    table->item(PC,0)->setFont(font);
    
    font.setBold(true);
    table->item(newPC,0)->setFont(font);
    
    PC = newPC;
}

}
}
