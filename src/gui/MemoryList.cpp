/* This file is part of MAPS.
 *
 * MAPS is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * MAPS is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * MAPS.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "MemoryList.hpp"
#include <boost/range/algorithm.hpp>
#include <QtWidgets>
#include "../emulator/cpu.hpp"

namespace maps {
namespace gui {

MemoryList::~MemoryList() = default;

MemoryList::MemoryList(const emulator::MMU::Memory* memory,
                       QWidget* parent) :
    QWidget{parent},
    memory{memory},
    table(new QTableWidget(emulator::MMU::memsize, 1, this))
{
    font.setStyleHint(QFont::Monospace);

    for (std::size_t i = 0; i < memory->size(); ++i) {
        auto item = new QTableWidgetItem("0");
        item->setFont(font);
        item->setTextAlignment(Qt::AlignRight);
        table->setItem(i, 0, item);
        table->setVerticalHeaderItem(i, new QTableWidgetItem(QString::number(i)));
    }

    table->setFixedWidth(table->sizeHint().width()/1.5);
    table->horizontalHeader()->stretchLastSection();
    table->horizontalHeader()->hide();
    table->setEditTriggers(QAbstractItemView::NoEditTriggers);

    QVBoxLayout* layout = new QVBoxLayout{this};
    layout->addWidget(table);
    setLayout(layout);
    setFixedWidth(sizeHint().width());
}

void MemoryList::updateMemory()
{
    for (std::size_t i = 0; i < memory->size(); ++i) {
        auto item = table->item(i, 0);
        item->setText(QString::number((*memory)[i], base).toUpper());
    }

    table->setFixedWidth(table->size().width());
    setFixedWidth(sizeHint().width());
}

void MemoryList::setBase(int b)
{
    base = b;
    updateMemory();
}

} // namespace gui
} // namespace maps
