/* This file is part of MAPS.
 *
 * MAPS is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * MAPS is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * MAPS.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "RegisterList.hpp"
#include <QVBoxLayout>
#include <QLCDNumber>
#include <QApplication>
#include <QLabel>
#include <iostream>

namespace maps {
namespace gui {
    
RegisterList::~RegisterList() = default;

RegisterList::RegisterList(QWidget* parent) : QWidget{parent},
    ccr{new QLCDNumber(this)},
    layout{new QVBoxLayout{this}}
{
    // Make the registers closer together since they're visually 1 item.
    layout->setSpacing(0);

    for(std::size_t i = 0; i < registers.size(); ++i) {
        auto& reg = registers[i];
        reg = new QLCDNumber{this};
        reg->setHexMode();
        reg->setDigitCount(4);

        // Make numbers black.
        reg->setSegmentStyle(QLCDNumber::SegmentStyle::Flat);
        reg->setAutoFillBackground(true);
        QHBoxLayout* lay = new QHBoxLayout;
        lay->addWidget(new QLabel{QString{"R%1"}.arg(i), this});
        lay->addWidget(reg);
        layout->addLayout(lay);
    }

    ccr->setBinMode();
    ccr->setDigitCount(2);
    ccr->setSegmentStyle(QLCDNumber::SegmentStyle::Flat);
    QHBoxLayout* lay = new QHBoxLayout;
    lay->addWidget(new QLabel{"CCR", this});
    lay->addWidget(ccr);

    layout->addLayout(lay);
    setLayout(layout);
}

void RegisterList::updateRegisters(const std::array<std::uint16_t, 16>& regs, std::bitset<2> cpuccr)
{
    QPalette changedPalette;
    changedPalette.setColor(QPalette::Foreground, Qt::red);
    for (std::size_t i = 0; i < regs.size(); ++i) {
        std::uint16_t newVal = regs[i];
        std::uint16_t oldVal = registers[i]->value();
        if (oldVal != newVal) {
            registers[i]->setPalette(changedPalette);
        } else {
            registers[i]->setPalette({});
        }
        registers[i]->display(regs[i]);
    }
    ccr->display(QString::fromStdString(cpuccr.to_string()));
}

void RegisterList::setBin()
{
    for (auto& reg :  registers) {
        reg->setBinMode();
        reg->setDigitCount(16);
        reg->setFixedWidth(reg->sizeHint().width());
    }
}

void RegisterList::setDec()
{
    for (auto& reg : registers) {
        reg->setDecMode();
        reg->setDigitCount(5);
        reg->setFixedWidth(reg->sizeHint().width());
    }
}

void RegisterList::setHex()
{
    for (auto& reg : registers) {
        reg->setHexMode();
        reg->setDigitCount(4);
        reg->setFixedWidth(reg->sizeHint().width());
    }
}

void RegisterList::setOct()
{
    for (auto& reg : registers) {
        reg->setOctMode();
        reg->setDigitCount(6);
        reg->setFixedWidth(reg->sizeHint().width());
    }
}

} // namespace gui
} // namespace maps
