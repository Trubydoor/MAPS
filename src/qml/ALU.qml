import QtQuick 2.1

Rectangle {
    color: "white"
    border.color: "black"
    border.width: 1

    Connections {
        target: alu
        onActivated: {
            color = "red"
            cuDisplay.color = "white"
            mmuDisplay.color = "white"
        }
    }

    Text {
        anchors.centerIn: parent
        text: "ALU"
    }
}
