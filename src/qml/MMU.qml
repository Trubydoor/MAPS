import QtQuick 2.1

Rectangle {
    color: "white"
    border.color: "black"
    border.width: 1

    Connections {
        target: mmu
        onActivated: {
            color = "red"
            cuDisplay.color = "white"
            aluDisplay.color = "white"
        }
    }

    Text {
        anchors.centerIn: parent
        text: "MMU"
    }

}
