import QtQuick 2.1

Row {
    CU {
        id: cuDisplay
        width: 100
        height: 400
    }

    Column {
        ALU {
            id: aluDisplay
            width: 100
            height: 200
        }

        MMU {
            id: mmuDisplay
            width: 100
            height: 200
        }
    }
}
