import QtQuick 2.1

Rectangle {
    color: "white"
    border.color: "black"
    border.width: 1

    Connections {
        target: cu
        onActivated: {
            color = "red"
            aluDisplay.color = "white"
            mmuDisplay.color = "white"
        }
    }

    Text {
        anchors.centerIn: parent
        text: "CU"
    }
}
