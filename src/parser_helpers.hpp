/* This file is part of MAPS.
 *
 * MAPS is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * MAPS is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * MAPS.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once
#include <sstream>
#include <boost/range/any_range.hpp>
#include <boost/range/algorithm/find_first_of.hpp>
#include <boost/algorithm/string/trim.hpp>

namespace maps {

template <typename T>
using forward_range = boost::any_range<T, boost::forward_traversal_tag,
                            const T&, std::ptrdiff_t>;

class parser_error : public std::exception
{
private:
    std::string error;
public:
    parser_error(std::string what, forward_range<char> error_range)
    {
        auto tok_end = boost::find_first_of(error_range, "\n,");
        std::string error_string = std::string(error_range.begin(), tok_end);
        boost::trim(error_string);

        std::stringstream ss;
        ss << "Expecting " << what << " here:" << std::endl;
        ss << error_string << std::endl;
        this->error = ss.str();
    }
    virtual const char* what() const throw() override {return error.c_str();}
};

}
