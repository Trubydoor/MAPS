/* This file is part of MAPS.
 *
 * MAPS is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * MAPS is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * MAPS.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "compiler.hpp"
#include <boost/spirit/include/qi_parse.hpp>
#include <boost/spirit/include/qi_char.hpp>
#include <boost/spirit/include/qi_eol.hpp>
#include "parser.hpp"

namespace maps {
namespace language {

std::string compile(forward_range<char> input)
{
    namespace qi = boost::spirit::qi;

    // Skip over single line C++-style comments.
    auto single_line_comment = "//" >> *(qi::char_ - qi::eol);
    // Skip over multi line C-style comments.
    auto multi_line_comment = "/*" >> *(qi::char_ - "*/") >> "*/";
    // Skip over comments and whitespace.
    auto skip = qi::space | single_line_comment | multi_line_comment;

    // Get iterators to the beginning and end of input.
    auto first = boost::begin(input);
    auto last = boost::end(input);
    std::string ret;

    /* This parser actually parses the assembly language. It stores the
     * instructions in a vector of unsigneds so that they can be processed
     * by the CU.
     */
    Parser<forward_range<char>::iterator, decltype(skip)> parser;
    //BodyParser<forward_range<char>::iterator, decltype(skip)> parser;
    first = boost::begin(input);
    last = boost::end(input);

    try {
        bool r = qi::phrase_parse(first, last, parser, skip, ret);
        if (!r) {
            std::string s = "Unknown parse failure.\n";
            s += "If you see this message, please email your assembler code to ";
            s += "D.Truby@warwick.ac.uk so this bug can be fixed!";
            throw std::runtime_error(s);
        }
    } catch (qi::expectation_failure<forward_range<char>::iterator>& e) {
        int line_no = std::count(input.begin(), e.first, '\n');
        std::stringstream ss;
        ss << e.what_.tag << " on line " << ++line_no;
        throw parser_error(ss.str(), std::make_pair(e.first, e.last));
    }

    // Return the vector of unsigneds.
    return ret;
}

}
}
