/* This file is part of MAPS.
 *
 * MAPS is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * MAPS is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * MAPS.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once
#include <string>
#include <boost/spirit/include/qi.hpp>
#include <boost/spirit/include/phoenix.hpp>

namespace maps {
namespace language {

template <typename Iterator, typename Skipper>
struct Parser : boost::spirit::qi::grammar<Iterator, std::string(), Skipper>
{
    using Rule = boost::spirit::qi::rule<Iterator, std::string(), Skipper>;

    Rule function;
    Rule identifier;
    Rule program;
    Rule functions;
    Rule num;
    Rule declaration;
    Rule assignment;
    Rule call;
    Rule ret;
    Rule body;
    Rule fbody;
    Rule reg;
    Rule reg_number;
    Rule statement;
    Rule condition;
    Rule ifstmt;

    std::vector<std::string> vars;

    Parser() : Parser::base_type{program, "language"}
    {
        using namespace boost::spirit::qi;
        using boost::phoenix::push_back;

        num %= raw[("0x" > hex[_pass = _1 <= 0xff]) |
               ("0o" > oct[_pass = _1 <= 0xff]) |
               ("0b" > bin[_pass = _1 <= 0xff]) |
               uint_[_pass = _1 <= 0xff]];
        num.name("numeric literal");
        
        reg_number %= raw[uint_ [_pass = _1 < 16]];
        // This rule exists solely so we can get better error messages.
        reg_number.name("register number");
        
        reg = no_case["r"] >> (reg_number[_val = std::string("r") + _1] | no_case["sp"][_val = "rsp"]
                | no_case["ar"][_val = "rar"] | no_case["vr"][_val = "rvr"]);
        reg.name("register");

        declaration = reg[_val = _1] >> ":=" >
                    (num[_val = "mov "+_1+","+_val] |
                    call[_val = _1 + "\n" + "mov rvr, "+_val]);;
        declaration.name("Variable Declaration");

        assignment = reg[_val = _1] >> "=" >
                    (num[_val = "mov "+_1+","+_val] |
                    call[_val = _1 + "\n" + "mov rvr, " + _val]);
                    
        condition = (reg >> "&&" >> reg)[_val = "and r12, " + _1 + ", " + _2] | 
                    (reg >> "||" >> reg)[_val = "or r12, " + _1 + ", " + _2] | 
                    ("!" >> reg)[_val = "not r12, " + _1];
        
        ifstmt = (lit("if") >> "(" >> condition >> ")" >> "{" >> body >> "}")
                [_val = _1 + "\nbgt after\n" + _2 + "\nafter:\n"];
                    
        statement %= (assignment > ";") | (call > ";") | ifstmt;

        call = (identifier >> "()")[_val = "jmp " + _1];
        
        ret = "return" >> (reg | num)[_val = "mov "+_1+","+"rvr\n" +
                    "jmp rar"] >> ";";
        ret.name("Return statement");
        
        body = *(statement[_val += _1 + "\n"]);

        fbody = body[_val = _1] > ret[_val += _1];
        fbody.name("Function body");
        
        function = ("fn" > identifier > "()" > "{" > fbody > "}")
                    [_val = "\n" + _1 + ":\n" + _2 + "\n"];

        identifier = alpha[push_back(_val, _1)] > *alnum[push_back(_val, _1)];
        identifier.name("identifier");

        functions = (*function[_val += _1]);

        program = functions[_val = "mov 2, rar\njmp main\njmp end\n", _val += _1, _val += "\nend:"];
    }
};

}
}
