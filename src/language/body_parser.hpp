/* This file is part of MAPS.
 *
 * MAPS is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * MAPS is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * MAPS.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once
#include <string>
#include <boost/spirit/include/qi.hpp>
#include <boost/spirit/include/phoenix.hpp>
#include <boost/lexical_cast.hpp>

namespace maps {
namespace language {

template <typename Iterator, typename Skipper>
struct BodyParser : boost::spirit::qi::grammar<Iterator, std::string(), Skipper>
{
    using Rule = boost::spirit::qi::rule<Iterator, std::string(), Skipper>;

    Rule declaration;
    Rule assignment;
    Rule identifier;
    Rule num;
    Rule body;
    Rule ret;
    Rule retval;
    Rule call;
    Rule assign_call;
    Rule statement;

    std::vector<std::string> vars;

    BodyParser() : BodyParser::base_type{body, "language"}
    {
        using namespace boost::spirit::qi;
        using boost::phoenix::push_back;
        namespace phx = boost::phoenix;

        num %= raw[("0x" > hex[_pass = _1 <= 0xff]) |
               ("0o" > oct[_pass = _1 <= 0xff]) |
               ("0b" > bin[_pass = _1 <= 0xff]) |
               uint_[_pass = _1 <= 0xff]];
        num.name("numeric literal");

        identifier = alpha[push_back(_val, _1)] >> *alnum[push_back(_val, _1)];
        identifier.name("identifier");

        declaration = identifier[_val = _1] >> ":=" >
                    (num[_val = "mov "+_1+","+_val] |
                    call[_val = _1 + "\n" + "mov rvr, "+_val]);
        declaration.name("Variable Declaration");

        assignment = identifier[_val = _1] >> "=" >
                    (num[_val = "mov "+_1+","+_val] |
                    call[_val = _1 + "\n" + "mov rvr, " + _val]);

        call = (identifier >> "()")[_val = "jmp " + _1];

        retval %= identifier | num;

        ret = "return" > retval[_val = "mov "+_1+","+"rvr\n" +
                    "jmp rar"] >> ";";
        ret.name("Return statement");

        statement %= declaration | call;

        body = *(statement[_val += _1 + "\n"] > ";") > ret[_val += _1];
        body.name("Function body");
    }
};

}
}
