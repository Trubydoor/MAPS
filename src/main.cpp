/* This file is part of MAPS.
 *
 * MAPS is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * MAPS is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * MAPS.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <QApplication>
#include <QtDebug>
#include "gui/MainWindow.hpp"
#include "language/compiler.hpp"

int main(int argc, char** argv) try
{
    QApplication app{argc, argv};
    maps::gui::MainWindow w;
    w.show();

    return app.exec();
} catch (std::exception& e) {
    qCritical() << e.what();
} catch (...) {} // Prevent random exceptions from leaving main.
