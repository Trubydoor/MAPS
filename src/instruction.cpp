/* This file is part of MAPS.
 *
 * MAPS is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * MAPS is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * MAPS.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "instruction.hpp"
#include <iostream>
#include <string>
#include <sstream>

namespace maps {

std::ostream& operator<<(std::ostream& os, Instruction in)
{
    std::string instruction_str;
    switch (in) {
    case Instruction::CLEAR: instruction_str = "CLEAR"; break;
    case Instruction::ADD_L: instruction_str = "ADD_LIT"; break;
    case Instruction::ADD_R: instruction_str = "ADD_REG"; break;
    case Instruction::SUB_L: instruction_str = "SUB_LIT"; break;
    case Instruction::SUB_R: instruction_str = "SUB_REG"; break;
    case Instruction::JMP: instruction_str = "JMP"; break;
    case Instruction::JMP_R: instruction_str = "JMP_REG"; break;
    case Instruction::BOZ: instruction_str = "BOZ"; break;
    case Instruction::BNE: instruction_str = "BNE"; break;
    case Instruction::LOAD: instruction_str = "LOAD"; break;
    case Instruction::STORE: instruction_str = "STORE"; break;
    case Instruction::MOV: instruction_str = "MOV"; break;
    case Instruction::MOV_L: instruction_str = "MOV_LIT"; break;
    case Instruction::AND: instruction_str = "AND"; break;
    case Instruction::OR: instruction_str = "OR"; break;
    case Instruction::NOT: instruction_str = "NOT"; break;
    }

    os << instruction_str;
    return os;
}

std::string to_string(Instruction in)
{
    std::stringstream ss;
    ss << in;
    return ss.str();
}

} // namespace maps
