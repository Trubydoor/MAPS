/* This file is part of MAPS.
 *
 * MAPS is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * MAPS is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * MAPS.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "controlunit.hpp"
#include "alu.hpp"
#include "mmu.hpp"

namespace maps {
namespace emulator {

bool ControlUnit::fetch()
{
    emit activated();
    if (cpu.PC >= cpu.instruction_memory.size()) {
        return false;
    }
    std::size_t PC = cpu.PC;
    cpu.PC++;
    opcode = cpu.instruction_memory[PC];
    return true;
}

void ControlUnit::decode()
{
    std::uint16_t op = opcode >> 12;
    value = opcode & 0xfff;

    instruction = static_cast<Instruction>(op);
}

void ControlUnit::execute()
{
    switch(instruction) {
    case Instruction::CLEAR: clear(); break;
    case Instruction::ADD_L: add_l(); break;
    case Instruction::ADD_R: add_d(); break;
    case Instruction::SUB_L: sub_l(); break;
    case Instruction::SUB_R: sub_d(); break;
    case Instruction::JMP: jmp(); break;
    case Instruction::JMP_R: jmp_r(); break;
    case Instruction::BOZ: boz(); break;
    case Instruction::BNE: bne(); break;
    case Instruction::LOAD: load(); break;
    case Instruction::STORE: store(); break;
    case Instruction::MOV: mov(); break;
    case Instruction::MOV_L: mov_l(); break;
    case Instruction::AND: bit_and(); break;
    case Instruction::OR: bit_or(); break;
    case Instruction::NOT: bit_not(); break;
    }
}

void ControlUnit::clear()
{
    unsigned reg = value & 0xf;
    cpu.R[reg] = 0;
    cpu.CCR.reset();
}

void ControlUnit::add_l()
{
    std::uint16_t val = value & 0xff;
    std::uint16_t reg = value >> 8;
    cpu.R[reg] = cpu.getALU().add(cpu.R[reg], val);
    if (cpu.R[reg] == 0) {
        cpu.CCR[1] = 0;
    } else {
        cpu.CCR[1] = 1;
    }
    cpu.CCR[0] = 0;
}

void ControlUnit::add_d()
{
    unsigned dest = value >> 8;
    unsigned reg1 = (value >> 4) & 0xf;
    unsigned reg2 = value & 0xf;
    cpu.R[dest] = cpu.getALU().add(cpu.R[reg1], cpu.R[reg2]);
    if (cpu.R[dest] == 0) {
        cpu.CCR[1] = 0;
    } else {
        cpu.CCR[1] = 1;
    }
    cpu.CCR[0] = 0;
}

void ControlUnit::sub_l()
{
    std::uint16_t val = value & 0xff;
    std::uint16_t reg = value >> 8;
    cpu.CCR[0] = val > cpu.R[reg];
    cpu.R[reg] = cpu.getALU().sub(cpu.R[reg], val);
    if (cpu.R[reg] == 0) {
        cpu.CCR[1] = 0;
    } else {
        cpu.CCR[1] = 1;
    }
}

void ControlUnit::sub_d()
{
    std::uint16_t dest = value >> 8;
    std::uint16_t reg1 = (value >> 4) & 0xf;
    std::uint16_t reg2 = value & 0xf;
    cpu.CCR[0] = cpu.R[reg2] > cpu.R[reg1];
    cpu.R[dest] = cpu.getALU().sub(cpu.R[reg1], cpu.R[reg2]);
    if (cpu.R[dest] == 0) {
        cpu.CCR[1] = 0;
    } else {
        cpu.CCR[1] = 1;
    }
}

void ControlUnit::jmp()
{
    cpu.PC = value;
}

void ControlUnit::jmp_r()
{
    std::uint16_t addr_reg = value & 0xf;
    cpu.PC = cpu.R[addr_reg];
}

void ControlUnit::bne()
{
    if (cpu.CCR[0] == 1) {
        cpu.PC = value;
    }
}

void ControlUnit::boz()
{
    if (cpu.CCR[1] == 0) {
        cpu.PC = value;
    }
}

void ControlUnit::load()
{
    unsigned loc = value & 0xf;
    unsigned reg = value >> 4;
    cpu.R[reg] = cpu.getMMU().load(cpu.R[loc]++);
}

void ControlUnit::store()
{
    unsigned loc = value & 0xf;
    unsigned reg = value >> 4;
    cpu.getMMU().store(--cpu.R[loc], cpu.R[reg]);
}

void ControlUnit::mov()
{
    std::uint16_t src = value >> 4;
    std::uint16_t dest = value & 0xf;
    cpu.R[dest] = cpu.R[src];
    if (cpu.R[dest] == 0) {
        cpu.CCR[1] = 0;
    }
    else {
        cpu.CCR[1] = 1;
    }
    cpu.CCR[0] = 0;
}

void ControlUnit::mov_l()
{
    std::uint16_t val = value >> 4;
    std::uint16_t dest = value & 0xf;
    cpu.R[dest] = val;
    if (cpu.R[dest] == 0) {
        cpu.CCR[1] = 0;
    }
    else {
        cpu.CCR[1] = 1;
    }
    cpu.CCR[0] = 0;
}

void ControlUnit::bit_and()
{
    std::uint16_t dest = value >> 8;
    std::uint16_t reg1 = (value >> 4) & 0xf;
    std::uint16_t reg2 = value & 0xf;

    cpu.R[dest] = cpu.getALU().bit_and(cpu.R[reg1],cpu.R[reg2]);
    cpu.CCR[1] = cpu.R[dest] == 0;
    cpu.CCR[0] = 0;
}

void ControlUnit::bit_or()
{
    std::uint16_t dest = value >> 8;
    std::uint16_t reg1 = (value >> 4) & 0xf;
    std::uint16_t reg2 = value & 0xf;

    cpu.R[dest] = cpu.getALU().bit_or(cpu.R[reg1], cpu.R[reg2]);
    cpu.CCR[1] = cpu.R[dest] == 0;
    cpu.CCR[0] = 0;
}


void ControlUnit::bit_not()
{
    std::uint16_t dest = (value >> 4) & 0xf;
    std::uint16_t reg1 = value & 0xf;

    cpu.R[dest] = cpu.getALU().bit_not(cpu.R[reg1]);
    cpu.CCR[1] = cpu.R[dest] == 0;
    cpu.CCR[0] = 0;
}
} // namespace emulator
} // namespace maps
