/* This file is part of MAPS.
 *
 * MAPS is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * MAPS is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * MAPS.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "alu.hpp"
namespace maps {
namespace emulator {

std::uint16_t ALU::add(const std::uint16_t lhs, const std::uint16_t rhs)
{
    emit activated();
    return lhs + rhs;
}

std::uint16_t ALU::sub(const std::uint16_t lhs, const std::uint16_t rhs)
{
    emit activated();
    return lhs - rhs;
}

std::uint16_t ALU::bit_and(const std::uint16_t lhs, const std::uint16_t rhs)
{
    emit activated();
    return lhs & rhs;
}

std::uint16_t ALU::bit_or(const std::uint16_t lhs, const std::uint16_t rhs)
{
    emit activated();
    return lhs | rhs;
}

std::uint16_t ALU::bit_not(const std::uint16_t val)
{
    return ~val;
}
} // namespace emulator
} // namespace maps
