/* This file is part of MAPS.
 *
 * MAPS is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * MAPS is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * MAPS.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "cpu.hpp"

#include "alu.hpp"
#include "controlunit.hpp"
#include "mmu.hpp"

namespace maps {
namespace emulator {

CPU::~CPU() = default;

CPU::CPU() : cu{new ControlUnit{*this}},
    alu{new ALU},
    mmu{new MMU}
{
    reset();
}

void CPU::reset()
{
    for (auto& x : R) {
        x = 0;
    }
    CCR.reset();
}

ControlUnit& CPU::getCU()
{
    return *cu;
}

ALU& CPU::getALU()
{
    return *alu;
}

MMU& CPU::getMMU()
{
    return *mmu;
}

} // namespace emulator
} // namespace maps
