set(sources cpu.cpp alu.cpp controlunit.cpp mmu.cpp)
set(headers cpu.hpp alu.hpp controlunit.hpp mmu.hpp)

add_library(emulator ${sources} ${headers})
target_link_libraries(emulator Qt5::Core)
