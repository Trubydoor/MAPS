/* This file is part of MAPS.
 *
 * MAPS is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * MAPS is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * MAPS.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once
#include <vector>
#include <cstdint>
#include <QObject>

#include "../instruction.hpp"
#include "cpu.hpp"

namespace maps {
namespace emulator {

class ControlUnit : public QObject
{
    Q_OBJECT
    CPU& cpu;
    std::uint16_t opcode {0};
    std::uint16_t value {0};

    Instruction instruction;

public:
    ControlUnit(CPU& cpu) : cpu(cpu) {}
    
    std::uint16_t getValue() {return value;}
    std::uint16_t getOpcode() {return opcode;}
    Instruction getInstruction() {return instruction;}

    bool fetch();
    void decode();
    void execute();

private:
    void clear();
    void add_l();
    void add_d();
    void sub_l();
    void sub_d();
    void jmp();
    void jmp_r();
    void boz();
    void bne();
    void load();
    void store();
    void mov();
    void mov_l();
    void bit_and();
    void bit_or();
    void bit_not();
    
signals:
    void activated();
};

} // namespace emulator
} // namespace maps
