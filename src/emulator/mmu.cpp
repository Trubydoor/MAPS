/* This file is part of MAPS.
 *
 * MAPS is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * MAPS is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * MAPS.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "mmu.hpp"
#include <boost/range/algorithm.hpp>

namespace maps {
namespace emulator {

std::uint16_t MMU::load(std::uint16_t loc)
{
    emit activated();
    return memory[loc];
}

void MMU::store(std::uint16_t loc, std::uint16_t val)
{
    emit activated();
    memory[loc] = val;
}

}
}
