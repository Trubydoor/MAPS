/* This file is part of MAPS.
 *
 * MAPS is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * MAPS is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * MAPS.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once
#include <memory>
#include <array>
#include <vector>
#include <cstdint>
#include <bitset>
#include <QObject>

namespace maps {
namespace emulator {

class ControlUnit;
class ALU;
class MMU;

class CPU
{
    std::unique_ptr<ControlUnit> cu;
    std::unique_ptr<ALU> alu;
    std::unique_ptr<MMU> mmu;

public:
    CPU();
    ~CPU();

    ControlUnit& getCU();
    ALU& getALU();
    MMU& getMMU();

    std::array<std::uint16_t, 16> R;
    std::vector<std::uint16_t> instruction_memory;
    std::vector<std::uint16_t>::size_type PC {0};
    std::bitset<2> CCR; // bit 0 of this CCR is the N bit and bit 1 is the Z bit.

    void reset();
};

} // namespace emulator
} // namespace maps
