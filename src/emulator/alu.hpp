/* This file is part of MAPS.
 *
 * MAPS is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * MAPS is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * MAPS.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once
#include <cstdint>
#include <QObject>

namespace maps {
namespace emulator{

class ALU : public QObject
{
    Q_OBJECT
public:
    std::uint16_t add(const std::uint16_t lhs, const std::uint16_t rhs);
    std::uint16_t sub(const std::uint16_t lhs, const std::uint16_t rhs);
    std::uint16_t bit_and(const std::uint16_t lhs, const std::uint16_t rhs);
    std::uint16_t bit_or(const std::uint16_t lhs, const std::uint16_t rhs);
    std::uint16_t bit_not(const std::uint16_t val);

signals:
    void activated();
};

} // namespace emulator
} // namespace maps
