/* This file is part of MAPS.
 *
 * MAPS is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * MAPS is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * MAPS.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once
#include <array>
#include <limits>
#include <cstdint>
#include <QObject>

namespace maps {
namespace emulator {

class MMU : public QObject
{
    Q_OBJECT

public:
    static const std::size_t memsize = 0xffff+1;
    using Memory = std::array<std::uint16_t, memsize>;

    std::uint16_t load(std::uint16_t loc);
    void store(std::uint16_t loc, std::uint16_t val);
    const Memory& getMemory() {return memory;}

signals:
    void activated();

private:
    Memory memory = {{0}};
};

}
}
