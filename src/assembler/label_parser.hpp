/* This file is part of MAPS.
 *
 * MAPS is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * MAPS is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * MAPS.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once
#include <unordered_map>
#include <string>
#include <boost/spirit/include/qi.hpp>
#include <boost/spirit/include/phoenix.hpp>

namespace maps {
namespace assembler {
using LabelMap = std::unordered_map<std::string, unsigned>;

template <typename Iterator, typename Skipper>
struct LabelParser : boost::spirit::qi::grammar<Iterator, LabelMap(), Skipper>
{
    // Production representing a label.
    boost::spirit::qi::rule<Iterator, std::string(), Skipper> label;
    // Production representing anything that isn't a label.
    boost::spirit::qi::rule<Iterator, void(), Skipper> other;
    // Production representing an empty line.
    boost::spirit::qi::rule<Iterator, void(), Skipper> empty;

    // Production representing the entire file.
    boost::spirit::qi::rule<Iterator,
           LabelMap(), Skipper> program;

    // The memory address representing the current line.
    unsigned mem_address = 0;

    LabelParser() : LabelParser::base_type(program)
    {
        using namespace boost::spirit::qi;
        using boost::phoenix::push_back;
        using boost::phoenix::ref;

        // A label is any string of 1 or more alphanumeric characters, followed by a ':'
        label = +alnum[push_back(_val, _1)] >> ':';
        label.name("label");
        empty = "";
        // Skip any other character except eol.
        other = +(char_ - eol);

        // If the line is a label, add it to the symbol table with the current memory address.
        program = +((label[_val[_1] = ref(mem_address)] | other[++ref(mem_address)] | empty) >> eol);
    }
};

} // namespace assembler
} // namespace maps
