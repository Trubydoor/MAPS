/* This file is part of MAPS.
 *
 * MAPS is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * MAPS is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * MAPS.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "disassembler.hpp"
#include <sstream>

namespace maps {
namespace assembler {

std::tuple<Instruction,std::string> disassemble(std::uint16_t opcode)
{
    std::stringstream retStr;
    Instruction instruction = static_cast<Instruction>(opcode >> 12);
    
    auto threeReg = [&]{
        retStr << "r" << ((opcode >> 8)  & 0xf) << ", ";
        retStr << "r" << ((opcode >> 4) & 0xf) << ", ";
        retStr << "r" << (opcode & 0xf);
    };
    
    auto litArith = [&]{
        retStr << "r" << ((opcode >> 8) & 0xf) << ", ";
        retStr << (opcode & 0xff);
    };
    
    auto reg = [&]{
        retStr << "r" << (opcode & 0xf);
    };
    
    auto branch = [&]{
        retStr << (opcode & 0xfff);
    };
    
    auto mem = [&]{
        retStr << "r" << ((opcode >> 4) & 0xf) << ", ";
        retStr << "r" << (opcode & 0xf);
    };
    
    switch (instruction) {
    case Instruction::CLEAR: reg(); break;
    case Instruction::ADD_L: litArith(); break;
    case Instruction::ADD_R: threeReg(); break;
    case Instruction::SUB_L: litArith(); break;
    case Instruction::SUB_R: threeReg(); break;
    case Instruction::JMP: branch(); break;
    case Instruction::JMP_R: reg(); break;
    case Instruction::BOZ: branch(); break;
    case Instruction::BNE: branch(); break;
    case Instruction::LOAD: mem(); break;
    case Instruction::STORE: mem(); break;
    case Instruction::MOV: { 
        retStr << "r" << ((opcode >> 4) & 0xf) << ", ";
        retStr << "r" << (opcode & 0xf);
        break;
    }
    case Instruction::MOV_L: {
        retStr << ((opcode >> 4) & 0xff) << ", ";
        retStr << "r" << (opcode & 0xf);
        break;
    }
    case Instruction::AND: threeReg(); break;
    case Instruction::OR: threeReg(); break;
    case Instruction::NOT: {
        retStr << "r" << ((opcode >> 4) & 0xf) << ",";
        retStr << "r" << (opcode & 0xf);
        break;
    }
    }
    
    return std::make_tuple(instruction,retStr.str());
}

}
}
