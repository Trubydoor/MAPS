/* This file is part of MAPS.
 *
 * MAPS is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * MAPS is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * MAPS.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once
#include <vector>
#include <string>
#include <cstdint>
#include <exception>
#include <boost/spirit/include/qi_grammar.hpp>
#include <boost/spirit/include/qi.hpp>
#include <boost/spirit/include/phoenix.hpp>
#include <boost/predef/compiler/clang.h>
#include "../instruction.hpp"
#include "label_parser.hpp"

// Clang reports sequence point errors on _val, which aren't strictly actually
// unsequenced accesses. Hence, we silence that warning for this file if compiling
// with clang
#if BOOST_COMP_CLANG
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wunsequenced"
#endif

namespace maps {
namespace assembler {

template <typename Iterator, typename Skipper>
struct Parser :
        boost::spirit::qi::grammar<Iterator, std::vector<std::uint16_t>(),
            Skipper>
{
    // A Mnemonic returns the opcode as an integer.
    using Mnemonic = boost::spirit::qi::rule<Iterator, std::uint16_t(),
            Skipper>;
    Mnemonic one_reg_mnemonic;
    Mnemonic branch_mnemonic;
    Mnemonic arith_mnemonic;
    Mnemonic three_reg_mnemonic;
    Mnemonic memory_mnemonic;
    Mnemonic move_mnemonic;
    Mnemonic jmp_mnemonic;
    Mnemonic cond_mnemonic;

    // Convenience references for the following lines.
    const boost::spirit::standard::no_case_type& no_case = boost::spirit::standard::no_case;
    const boost::spirit::_val_type& _val = boost::spirit::_val;

    Mnemonic CLEAR {no_case["CLEAR"][_val = Instruction::CLEAR]};
    Mnemonic JMP {no_case["JMP"][_val = Instruction::JMP]};
    Mnemonic BOZ {(no_case["BOZ"] | no_case["BEQ"])[_val = Instruction::BOZ]};
    Mnemonic BNE {(no_case["BNE"] | no_case["BMI"])[_val = Instruction::BNE]};
    Mnemonic ADD {no_case["ADD"][_val = Instruction::ADD_L]};
    Mnemonic SUB {no_case["SUB"][_val = Instruction::SUB_L]};
    Mnemonic LOAD {no_case["LOAD"][_val = Instruction::LOAD]};
    Mnemonic STORE {no_case["STORE"][_val = Instruction::STORE]};
    Mnemonic MOV {no_case["MOV"][_val = Instruction::MOV]};
    Mnemonic AND {no_case["AND"][_val = Instruction::AND]};
    Mnemonic OR {no_case["OR"][_val = Instruction::OR]};
    Mnemonic NOT {no_case["NOT"][_val = Instruction::NOT]};

    using Rule = Mnemonic;

    Rule one_reg;
    Rule branch;
    Rule jump_arg;
    Rule jump;
    Rule lit_arg;
    Rule move;
    Rule move_arg;
    Rule three_reg_arg;
    Rule arith_arg;
    Rule arith;
    Rule memory;
    Rule conditional;
    Rule rnot;

    Rule reg;
    Rule reg_number;
    Rule num;
    Rule line;                                              

    boost::spirit::qi::rule<Iterator, void(), Skipper> label;
    boost::spirit::qi::rule<Iterator, std::string(), Skipper> identifier;

    boost::spirit::qi::rule<Iterator, std::vector<std::uint16_t>(),
            Skipper> lines;
    boost::spirit::qi::rule<Iterator, std::vector<std::uint16_t>(),
            Skipper> program;

    LabelMap labels;

    Parser(LabelMap label_map) :
            Parser::base_type{program, "assembly"}, labels{std::move(label_map)}
    {
        using namespace boost::spirit::qi;
        namespace phx = boost::phoenix;
        using phx::push_back;

        // Instructions that operate on only 1 register
        one_reg_mnemonic %= CLEAR;
        // Instructions that operate on 1 label.
        branch_mnemonic %= BOZ | BNE;
        // Instructions that operate on 1 register and 1 literal.
        arith_mnemonic %= ADD | SUB;
        // Instructions that opreate on 1 memory address and 1 register.
        memory_mnemonic %= LOAD | STORE;
        move_mnemonic %= MOV;
        jmp_mnemonic %= JMP;
        cond_mnemonic %= AND | OR;

        // num ::= "0x" hex | "0o" oct | "0b" bin | uint
        num %= ("0x" > hex[_pass = _1 <= 0xff]) |
               ("0o" > oct[_pass = _1 <= 0xff]) |
               ("0b" > bin[_pass = _1 <= 0xff]) |
               uint_[_pass = _1 <= 0xff];
        num.name("numeric literal");

        // All of the following rules put the mnemonic in the 4 most significant
        // bits. This is done with the rule _val = _1 << 12.

        one_reg = (one_reg_mnemonic > reg)
                //Put the value in the last 4 bits.
                [_val = _1 << 12, _val += _2];

        // This function gives us the number of labels that match the identifier.
        // If it's 0, then the label doesn't exist and we force a failure.
        auto label_count = phx::bind(&LabelMap::count, labels, _1);
        branch = (branch_mnemonic > identifier [_pass = label_count != 0])
                // Get the label from the symbol table and add it to the end.
                [_val = _1 << 12, _val += phx::ref(labels)[_2]];

        jump_arg = identifier[_pass = label_count != 0, _val = phx::ref(labels)[_1]] |
                reg[_val = 1 << 12, _val += _1];

        jump = (jmp_mnemonic > jump_arg)[_val = _1 << 12, _val += _2];

        lit_arg %= num;

        three_reg_arg = reg[_val = _1 << 4] > "," > reg[_val += _1];

        arith_arg = lit_arg[_val = _1] | three_reg_arg[_val = 1 << 12, _val += _1];
        arith_arg.name("register or unsigned integer literal");

        arith = arith_mnemonic[_val = _1 << 12] > reg[_val += _1 << 8] > "," >
            (arith_arg[_val += _1]);

        memory = (memory_mnemonic[_val = _1 << 12] >
            reg[_val += _1 << 4] > "," > reg[_val += _1]);

        move_arg = (num[_val += _1 << 4, _val += 1 << 12] > "," > reg[_val += _1]) |
                   (reg[_val += _1 << 4] > "," > reg[_val += _1]);

        move = move_mnemonic[_val = _1 << 12] >> move_arg[_val += _1];
        conditional = cond_mnemonic[_val = _1 << 12] >
                reg[_val += _1 << 8] > "," >
                reg[_val += _1 << 4] > "," > reg[_val += _1];
                
        rnot = NOT[_val = _1 << 12] > reg[_val += _1 << 4] > "," > 
                reg[_val += _1];
                
        // identifier ::= ("a-z0-9")*
        identifier = +alnum [push_back(_val,_1)];
        // label ::= ("a-z0-9")* ":"
        label = +alnum >> ':';
                    label.name("label");

        // reg_number ::= uint
        reg_number = uint_ [_pass = _1 < 16, _val = _1];
        // This rule exists solely so we can get better error messages.
        reg_number.name("register number");

        // reg ::= "D" uint
        reg = no_case["r"] > (reg_number [_val = _1] | no_case["sp"][_val = 15]
                | no_case["ar"][_val = 14] | no_case["vr"][_val = 13]);
        reg.name("register");

        // line ::= one_reg | branch | move | lit_arith | three_reg_arith | memory
        line %= one_reg | branch | move | arith | memory | move | jump | conditional | rnot;
        line.name("line");

        // lines ::= ((label (line)? | line | "") eol)*
        lines = *(label >> -(line[push_back(_val, _1)]) |
                  line[push_back(_val, _1)] |
                  ""
                >> eol);
        lines.name("lines");

        // program ::= lines HALT eol
        program = lines[_val = _1] > *eol;
        program.name("assembly");

        identifier.name("jump target");

        on_error<rethrow>
        (
            program
          , _4
        );
    }

};
}
}

#if BOOST_COMP_CLANG
#pragma clang diagnostic pop
#endif
