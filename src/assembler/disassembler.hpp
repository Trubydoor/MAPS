/* This file is part of MAPS.
 *
 * MAPS is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * MAPS is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * MAPS.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once
#include <tuple>
#include <string>
#include <cstdint>
#include "../instruction.hpp"

namespace maps {
namespace assembler {
    
/*!
 * \brief Disassemble the given opcode into its constituent parts.
 * \param instruction The instruction to disassemble.
 * \return A pair with an instruction type and a string containing the arguments.
 */
std::tuple<Instruction,std::string> disassemble(std::uint16_t instruction);

}
}
