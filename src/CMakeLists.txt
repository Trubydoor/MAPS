set(sources main.cpp instruction.cpp)
set(headers instruction.hpp parser_helpers.hpp)

qt5_add_resources(resources resources.qrc)
add_executable(maps WIN32 ${sources} ${headers} ${resources})

add_subdirectory(emulator)
add_subdirectory(assembler)
add_subdirectory(language)
add_subdirectory(gui)

## Install paths.
target_link_libraries(maps gui assembler emulator language)
install(TARGETS maps
    BUNDLE DESTINATION . COMPONENT Runtime
    RUNTIME DESTINATION bin COMPONENT Runtime)

## Set install paths for BundleInstall.
set(plugin_dest_dir bin)
set(APPS "\${CMAKE_INSTALL_PREFIX}/bin/maps")
if (APPLE)
    set(plugin_dest_dir QtTest.app/Contents/MacOS)
    set(APPS "\${CMAKE_INSTALL_PREFIX}/maps.app")
endif(APPLE)
if (WIN32)
    set(APPS "\${CMAKE_INSTALL_PREFIX}/bin/maps.exe")
endif(WIN32)

## Get the plugin directory.
list(GET Qt5Gui_PLUGINS 0 PLUGIN)
get_target_property(QT5_PLUGINS_DIR ${PLUGIN} LOCATION)
get_filename_component(QT5_PLUGINS_DIR ${QT5_PLUGINS_DIR} PATH)
get_filename_component(QT5_PLUGINS_DIR ${QT5_PLUGINS_DIR} PATH)

## Get the plugin libraries.
FILE(GLOB PLUGIN_LINKS / "${QT5_PLUGINS_DIR}/platforms/*")

## Remove symlinks from the plugin libraries
set(plugins)
foreach(file ${PLUGIN_LINKS})
    get_filename_component(file ${file} REALPATH)
    list(APPEND plugins ${file})
endforeach()

## Install the plugin libraries where cmake can find them at runtime.
INSTALL(FILES ${plugins} DESTINATION ${plugin_dest_dir}/platforms COMPONENT Runtime)
## Tell BundleInstall where to find Qt's libraries.
set(DIRS ${QT_LIBRARY_DIRS})

INSTALL(CODE "
    file(GLOB_RECURSE QTPLUGINS
      \"\${CMAKE_INSTALL_PREFIX}/${plugin_dest_dir}/plugins/*${CMAKE_SHARED_LIBRARY_SUFFIX}\")
    include(BundleUtilities)
    fixup_bundle(\"${APPS}\" \"\" \"${DIRS}\")
    " COMPONENT Runtime)
