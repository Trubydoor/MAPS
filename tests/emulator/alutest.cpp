/* This file is part of MAPS.
 *
 * MAPS is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * MAPS is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * MAPS.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "alutest.hpp"
#include <random>
#include <string>
#include <boost/range/irange.hpp>

QTEST_MAIN(maps::emulator::tests::ALUTest)

namespace maps {
namespace emulator {
namespace tests {

void ALUTest::data()
{
    QTest::addColumn<std::uint16_t>("lhs");
    QTest::addColumn<std::uint16_t>("rhs");

    std::mt19937 rng {std::random_device{}()};
    std::uniform_int_distribution<std::uint16_t> rand;

    for (int i : boost::irange(1,11)) {
        QTest::newRow(std::to_string(i).c_str()) << rand(rng) << rand(rng);
    }
}

void ALUTest::add()
{
    QFETCH(std::uint16_t, lhs);
    QFETCH(std::uint16_t, rhs);
    std::uint16_t ans = lhs + rhs;
    QCOMPARE(alu.add(lhs,rhs), ans);
}

void ALUTest::sub()
{
    QFETCH(std::uint16_t, lhs);
    QFETCH(std::uint16_t, rhs);
    std::uint16_t ans  = lhs - rhs;
    QCOMPARE(alu.sub(lhs, rhs), ans);
}

}
}
}

#include "alutest.moc"
