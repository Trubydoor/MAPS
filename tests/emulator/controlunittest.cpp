/* This file is part of MAPS.
 *
 * MAPS is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * MAPS is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * MAPS.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "controlunittest.hpp"

#include <sstream>
#include <boost/range/irange.hpp>
#include "../../src/assembler/assembler.hpp"
#include "../../src/emulator/mmu.hpp"

QTEST_MAIN(maps::emulator::tests::ControlUnitTest);

namespace maps {
namespace emulator {
namespace tests {

void ControlUnitTest::data()
{
    QTest::addColumn<std::uint16_t>("val1");
    QTest::addColumn<std::uint16_t>("val2");

    std::mt19937 rng {std::random_device{}()};
    std::uniform_int_distribution<std::uint16_t> rand{0, 0xff};

    for (int i : boost::irange(1,11)) {
        QTest::newRow(std::to_string(i).c_str()) << rand(rng) << rand(rng);
    }
}
    
void ControlUnitTest::init()
{
    cpu.reset(new CPU);
    cu.reset(new ControlUnit{*cpu});
}
    
void ControlUnitTest::fetch() try
{
    cpu->instruction_memory = maps::assembler::assemble("CLEAR R10");
    bool ret = cu->fetch();
    QVERIFY(ret);
    QCOMPARE(cu->getOpcode(), static_cast<std::uint16_t>(10));
} catch (std::exception& e) {
    QFAIL(e.what());
}

void ControlUnitTest::decode() try
{
    cpu->instruction_memory = maps::assembler::assemble("ADD R0, 10");
    cu->fetch();
    cu->decode();
    QCOMPARE(cu->getInstruction(), Instruction::ADD_L);
    QCOMPARE(cu->getValue(), static_cast<std::uint16_t>(10));
} catch (std::exception& e) {
    QFAIL(e.what());
}

void ControlUnitTest::execute() try
{
    cpu->instruction_memory = maps::assembler::assemble("ADD R0, 10");
    cu->fetch();
    cu->decode();
    cu->execute();
    QCOMPARE(cpu->R[0], static_cast<std::uint16_t>(10));
} catch (std::exception& e) {
    QFAIL(e.what());
}

void ControlUnitTest::clear() try
{
    cpu->instruction_memory = maps::assembler::assemble("CLEAR R0");
    cpu->R[0] = 200;
    QCOMPARE(cpu->R[0], static_cast<std::uint16_t>(200));
    cu->fetch();
    cu->decode();
    cu->execute();
    QCOMPARE(cpu->R[0], static_cast<std::uint16_t>(0));
} catch (std::exception& e) {
    QFAIL(e.what());
}

void ControlUnitTest::add_l() try
{
    QFETCH(std::uint16_t, val1);
    QFETCH(std::uint16_t, val2);
    cpu->R[1] = val1;

    std::stringstream ss;
    ss << "ADD R1, " << val2 << std::endl;

    cpu->instruction_memory = maps::assembler::assemble(ss.str());
    cu->fetch();
    cu->decode();
    cu->execute();
    std::uint16_t res = val1 + val2;
    QCOMPARE(cpu->R[1], res);
} catch (std::exception& e) {
    QFAIL(e.what());
}

void ControlUnitTest::add_r() try
{

    QFETCH(std::uint16_t, val1);
    QFETCH(std::uint16_t, val2);

    cpu->R[0] = val1;
    cpu->R[1] = val2;

    cpu->instruction_memory = maps::assembler::assemble("ADD R2, R0, R1\n");
    while(cu->fetch()) {
        cu->decode();
        cu->execute();
    }
    QCOMPARE(cpu->R[2], static_cast<std::uint16_t>(val1 + val2));
} catch (std::exception& e) {
    QFAIL(e.what());
}

void ControlUnitTest::sub_l() try
{
    QFETCH(std::uint16_t, val1);
    QFETCH(std::uint16_t, val2);
        
    cpu->R[3] = val1;

    std::stringstream ss;
    ss << "SUB R3, " << val2 << std::endl;
    cpu->instruction_memory = maps::assembler::assemble(ss.str());
    cu->fetch();
    cu->decode();
    cu->execute();
    QCOMPARE(cpu->R[3], static_cast<std::uint16_t>(val1-val2));
} catch (std::exception& e) {
    QFAIL(e.what());
}

void ControlUnitTest::sub_r() try
{
    QFETCH(std::uint16_t, val1);
    QFETCH(std::uint16_t, val2);
        
    cpu->R[2] = val1;
    cpu->R[3] = val2;

    cpu->instruction_memory = maps::assembler::assemble("SUB R4, R3, R2\n");
    run_cpu();

    QCOMPARE(cpu->R[4], static_cast<std::uint16_t>(val2 - val1));
} catch (std::exception& e) {
    QFAIL(e.what());
}

void ControlUnitTest::boz() try
{
    QFETCH(std::uint16_t, val1);
    QFETCH(std::uint16_t, val2);
    cpu->R[9] = val1;
    cpu->R[10] = val2;

    std::stringstream ss;
    ss << "loop: " << std::endl;
    ss << "add r10, 1" << std::endl;
    ss << "sub r9, 1" << std::endl;
    ss << "boz end" << std::endl;
    ss << "jmp loop" << std::endl;
    ss << "end:" << std::endl;

    cpu->instruction_memory = maps::assembler::assemble(ss.str());
    run_cpu();

    std::uint16_t res = val1 + val2;
    QCOMPARE(cpu->R[10], res);
} catch (std::exception& e) {
    QFAIL(e.what());
}

void ControlUnitTest::mov_l() try
{
    QFETCH(std::uint16_t, val1);
    QFETCH(std::uint16_t, val2);
    cpu->R[13] = val1;
    cpu->R[14] = val2;
    cpu->instruction_memory = maps::assembler::assemble("MOV R13, R14\n");
    run_cpu();

    QCOMPARE(cpu->R[13], cpu->R[14]);
} catch (std::exception& e) {
    QFAIL(e.what());
}

} // namespace tests
} // namespace emulator
} // namespace maps

#include "controlunittest.moc"
