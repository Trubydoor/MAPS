/* This file is part of MAPS.
 *
 * MAPS is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * MAPS is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * MAPS.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CONTROLUNITTEST_HPP
#define CONTROLUNITTEST_HPP

#include <QTest>
#include "../../src/emulator/controlunit.hpp"
#include <memory>
#include <random>
#include <cstdint>
#include <QTime>

namespace maps {
namespace emulator {
namespace tests {

class ControlUnitTest : public QObject
{
    Q_OBJECT
    std::unique_ptr<CPU> cpu;
    std::unique_ptr<ControlUnit> cu;
    static void data();
private slots:
    void init();

    void fetch();
    void decode();
    void execute();

    void run_cpu() {
        while (cu->fetch()) {
            cu->decode();
            cu->execute();
        }
    }

    void clear();
    void clear_data() {data();}

    void add_l();
    void add_l_data() {data();}

    void add_r();
    void add_r_data() {data();}

    void sub_l();
    void sub_l_data() {data();}

    void sub_r();
    void sub_r_data() {data();}

    void boz();
    void boz_data() {data();}

    void mov_l();
    void mov_l_data() {data();}
};

} // namespace tests
} // namespace emulator
} // namespace maps

#endif // CONTROLUNITTEST_HPP
