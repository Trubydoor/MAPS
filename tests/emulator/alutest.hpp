/* This file is part of MAPS.
 *
 * MAPS is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * MAPS is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * MAPS.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ALUTEST_HPP
#define ALUTEST_HPP

#include <QtTest>
#include "alu.hpp"

namespace maps {
namespace emulator {
namespace tests {

class ALUTest : public QObject
{
    Q_OBJECT
    maps::emulator::ALU alu;
    static void data();
private slots:
    void add();
    void add_data() {data();}

    void sub();
    void sub_data() {data();}
};

} // namespace tests
} // namespace emulator
} // namespace tests

#endif // ALUTEST_HPP
